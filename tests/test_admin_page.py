from page_objects import AdminPage, SearchFilter
import allure


@allure.severity(allure.severity_level.CRITICAL)
@allure.feature('Admin Page')
@allure.story('Check the opening of the main page')
def test_open_home_page(browser):
    AdminPage(browser) \
        .open_admin_page(browser) \
        .check_custom_text_widget_orders()


@allure.severity(allure.severity_level.CRITICAL)
@allure.feature('Admin Page')
@allure.story('Go to subscription page')
def test_go_to_subscriptions_page(browser):
    AdminPage(browser) \
        .open_admin_page(browser) \
        .open_page_subscriptions() \
        .check_custom_text_widget_subscriptions()


@allure.severity(allure.severity_level.CRITICAL)
@allure.feature('Admin Page')
@allure.story('Go to subscription page and return to order page')
def test_go_to_orders_page(browser):
    AdminPage(browser) \
        .open_admin_page(browser) \
        .open_page_subscriptions() \
        .open_page_orders() \
        .check_custom_text_widget_orders()


@allure.severity(allure.severity_level.CRITICAL)
@allure.feature('Order status')
@allure.story('Display processing order')
def test_display_processing_orders(browser):
    AdminPage(browser) \
        .open_admin_page(browser)
    SearchFilter(browser) \
        .display_processing_order()


@allure.severity(allure.severity_level.CRITICAL)
@allure.feature('Order status')
@allure.story('Display completed order')
def test_display_completed_orders(browser):
    AdminPage(browser) \
        .open_admin_page(browser)
    SearchFilter(browser) \
        .display_completed_order()


@allure.severity(allure.severity_level.CRITICAL)
@allure.feature('Order status')
@allure.story('Display pending order')
def test_display_pending_orders(browser):
    AdminPage(browser) \
        .open_admin_page(browser)
    SearchFilter(browser) \
        .display_pending_order()


@allure.severity(allure.severity_level.CRITICAL)
@allure.feature('Order status')
@allure.story('Display failed order')
def test_display_failed_orders(browser):
    AdminPage(browser) \
        .open_admin_page(browser)
    SearchFilter(browser) \
        .display_failed_order()


@allure.severity(allure.severity_level.CRITICAL)
@allure.feature('Order status')
@allure.story('Display on hold order')
def test_display_on_hold_orders(browser):
    AdminPage(browser) \
        .open_admin_page(browser)
    SearchFilter(browser) \
        .display_on_hold_order()


@allure.severity(allure.severity_level.NORMAL)
@allure.feature('Search Product')
@allure.story('Search for a product by ID')
def test_search_order_by_id(browser):
    AdminPage(browser) \
        .open_admin_page(browser)
    SearchFilter(browser) \
        .search_order_by_id(id_order='11')


@allure.severity(allure.severity_level.NORMAL)
@allure.feature('Admin Page')
@allure.story('Go to the second page with the goods and return to the first')
def test_go_to_next_and_return_previous_page(browser):
    AdminPage(browser) \
        .open_admin_page(browser)
    SearchFilter(browser) \
        .go_to_next_previous_page(name_attribute='value')

@allure.severity(allure.severity_level.NORMAL)
@allure.feature('Admin Page')
@allure.story('Open details order')
def test_open_details_order(browser):
    AdminPage(browser) \
        .open_admin_page(browser) \
        .open_details_order(value='value')


@allure.severity(allure.severity_level.NORMAL)
@allure.feature('Admin Page')
@allure.story('Open details subscription')
def test_open_details_subscription(browser):
    AdminPage(browser) \
        .open_admin_page(browser) \
        .open_page_subscriptions() \
        .open_details_subscription(value='value')

