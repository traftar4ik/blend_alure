class Admin:
    class canvas_widgets_from_orders_page:
        custom_button_widget_subscriptions = {'css': 'div.appsmith_widget_05zt0tvvz9.button-class2 button'}

    class canvas_widgets_from_subscription_page:
        custom_button_widget_orders = {'css': 'div.appsmith_widget_aqy7miecaj.button-class1 button'}

    class custom_text_widget:
        custom_text_widget_orders = {'css': 'div.t--widget-textwidget.title span'}
        custom_text_widget_subscriptions = {'css': 'div.appsmith_widget_88p2jhtjnr.t--widget-textwidget span'}

    class order:
        button_order_details = {'css': '#tablenq2fwu0mxu > div.tableWrap > div > div.tbody > div:nth-child(1) > div:nth-child(6) > div > div > button'}

        year = {'css': 'div.current_year input[type=text].bp3-input'}

        iframe_orders = {'css': '#ajwj6freuo > div > iframe'}

        button_notes = {'css': 'div.appsmith_widget_qd1jinmvq3 div.CUSTOM_BUTTON_WIDGET button'}

        notes = {'css': 'div.CUSTOM_TEXT_WIDGET span'}

    class subscription:
        button_subscription_details = {'css': '#tablenq2fwu0mxu > div.tableWrap > div > div.tbody > div:nth-child(1) > div:nth-child(6) > div > div > button'}

        iframe_subscriptions = {'css': '#dcbtln449t > div > iframe'}

        year = {'css': 'div.appsmith_widget_q0x13uts4a input[type=text].bp3-input'}

        button_notes = {'css': 'div.appsmith_widget_ey59sz12md div.CUSTOM_BUTTON_WIDGET button'}

        notes = {'css': 'div.CUSTOM_TEXT_WIDGET span'}

