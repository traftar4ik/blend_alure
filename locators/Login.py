class Login:

    class go_to_login_user_page:
        select_user_account = {'xpath': '//*[@id="root"]/section/div/div/div/div/button[1]'}

    class user_login:
        email_field = {'css': '#username'}
        password_field = {'css': '#password'}
        login_button = {'css': '#kc-login'}


    class github_login:
        github_button = {'css': '#zocial-github'}
        email_github = {'css': '#login_field'}
        password_github = {'css': '#password'}
        sign_in_button = {'css': 'input[type=submit]'}
        autorize_spel = {'css': '#js-oauth-authorize-btn'}

    class gitlab_login:
        gitlab_button = {'css': '#zocial-gitlab'}
        email_gitlab = {'css': '#user_login'}
        password_gitlab = {'css': '#user_password'}
        sign_in_button = {'css': '#new_user > div.submit-container.move-submit-down > input'}
        autorize_spel = {'css': '#js-oauth-authorize-btn'}

